import socket
import json

class Socket:

    def __init__(self, host='localhost', port=22222):
        self.host = host
        self.port = port
        self.data = None
        self.s = None

    def connect(self, ip):
        print('conectando a ', ip)
        self.host = ip
        port = 22222
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.s.connect((self.host, port))
        except:
            self.s = None
        else:
            if self.data:
                self.send(self.data)

    def cerrar(self):
        self.s.close()

    def send(self, data):
        if self.s:
            try:
                self.s.send(json.dumps(data))
                print('enviado')
            except:
                self.s = None
                print('error, reconectar')
        else:
            self.data = data

    def mas_grande(self):
        self.send({
            't': 'Font',
            's': 1,
        })

    def menos_grande(self):
        self.send({
            't': 'Font',
            's': -1,
        })

    def abajo(self):
        self.send({
            't': 'Scroll',
            'y': -50,
        })

    def arriba(self):
        self.send({
            't': 'Scroll',
            'y': 50,
        })