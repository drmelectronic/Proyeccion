 #! /usr/bin/python
# -*- coding: utf-8 -*-
local = 0
import gtk
import Modulos
import Widgets
import os
import urllib3
import Chrome
import gobject
import json
import os
import time
import Sonido
import socket
import datetime
pid = os.getpid()
print 'PID', pid
if os.name == 'nt':
    import win32api
else:
    import sh
infinito = True
version = 0.1
dia = 'Actualización Viernes 29 de enero de 2016'
if local:
    localhost = 'localhost'
    #localhost = 'localhost'
    appengine_ip = appengine = localhost
    web = titulo = localhost
else:
    titulo = "Sistema Multimedia Palabra y Espíritu v%s" % version
    appengine_ip = appengine = 'iglesiapalabrayespiritu.appspot.com'
    web = 'www.palabrayespiritu.org'
    try:
        ips = socket.gethostbyname_ex(appengine)
    except socket.gaierror:
        pass
    else:
        print ips
        for ip in ips:
            if isinstance(ip, list) and len(ip) > 0:
                digit = True
                for n in ip[0].split('.'):
                 if not n.isdigit():
                    digit = False
                if digit:
                    appengine_ip = ip[0]
                    break

import webbrowser
try:
    webbrowser.get('google-chrome')
except:
    pass
try:
    webbrowser.get('firefox')
except:
    pass
try:
    webbrowser.get('opera')
except:
    pass
try:
    webbrowser.get('safari')
except:
    pass
try:
    webbrowser.get('windows-default')
except:
    pass
gobject.threads_init()

class Splash(gtk.Window):

    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_POPUP)
        self.set_position(gtk.WIN_POS_CENTER_ALWAYS)
        self.show_all()
        path = os.path.join('images', 'splash.png')
        pixbuf = gtk.gdk.pixbuf_new_from_file(path)
        pixmap, mask = pixbuf.render_pixmap_and_mask()
        width, height = pixmap.get_size()
        del pixbuf
        self.set_app_paintable(True)
        self.resize(width, height)
        self.realize()
        self.window.set_back_pixmap(pixmap, False)
        self.show_all()
        gobject.idle_add(self.aplicacion)

    def aplicacion(self, *args):
        self.a = Aplicacion()
        self.hide_all()

class Aplicacion:
    def __init__(self):
        self.grupo = gtk.WindowGroup()
        self.ventanas = []
        self.http = Http(self.ventanas)
        self.sessionid = None
        Chrome.init()
        self.ventana = self.salidas()
        self.login()

    def login(self, *args):
        dialog = Widgets.Login(self.http)
        s.hide_all()
        print dialog
        respuesta = dialog.iniciar()
        print respuesta
        print dialog.sessionid
        if respuesta:
            self.ventana.login(dialog.sessionid)
            self.sessionid = dialog.sessionid
            self.usuario = dialog.user
            self.password = dialog.pw
            print self.usuario
            dialog.cerrar()
        else:
            dialog.cerrar()

    def salidas(self, *args):
        global dia
        global version
        ventana = Modulos.Ventana(self, version, dia)
        self.grupo.add_window(ventana)
        self.ventanas.append(ventana)
        ventana.connect('cerrar', self.cerrar)
        ventana.connect('login', self.login)
        ventana.connect('salidas', self.salidas)
        if len(self.ventanas) > 1:
            ventana.login(self.sessionid)
        ventana.grab_focus()
        return ventana
    def cerrar(self, ventana):
        self.ventanas.remove(ventana)
        del ventana
        if len(self.ventanas) == 0:
            gtk.main_quit()

class Http:

    def __init__(self, ventanas):
        global appengine_ip
        global appengine
        global version
        self.conn = urllib3.HTTPConnectionPool(appengine_ip)
        self.ventanas = ventanas
        self.funciones = [self.nada]
        self.signals = {'update': self.funciones}
        self.server = '/musica/'
        self.dominio = appengine
        global web
        self.web = web
        self.csrf = ''
        #self.server = 'http://localhost:8000/despacho'
        self.login_ok = False
        self.timeout = None
        self.backup = True
        self.backup_urls = ('actualizar-tablas', 'solo-unidad', 'unidad-salida', 'datos-salida', 'flota-llegada')
        self.backup_dia = None
        #self.cookies = {'Cookie': {}}
        self.headers = {'Cookie': '', 'Origin': appengine, 'Host': appengine ,'Content-Type': 'application/x-www-form-urlencoded'}
        self.datos = {'rutas': (('Vacio', 0),),
            'despacho': None}
        self.username = ''
        self.password = ''
        self.sessionid = ''
        self.nombre = ''
        self.iglesia_id = None
        self.version = version
        self.despachador = None
        self.despachador_id = None
        self.unidad = {}
        self.salida = {}
        self.pagos = []
        self.castigos = []
        self.seriacion = []
        self.boletos_limites = []
        self.servicio = None
        self.grifo = False
        self.sonido = Sonido.Hilo()
        self.sonido.start()

    def login(self, usuario, clave):
        self.headers = {'Cookie': '', 'Origin': appengine, 'Host': appengine ,'Content-Type': 'application/x-www-form-urlencoded'}
        self.login_ok = False
        self.username = usuario
        self.clave = clave
        url = 'login'
        self.load(url)
        self.csrf = self.set_cookie('csrftoken')
        login = {
            'username': self.username,
            'clave': self.clave
            }
        if self.send_login(login):
            print 'Login'
        return self.sessionid

    def send_login(self, login):
        self.datos = self.load('login', login)
        if not self.datos:
            return False
        if not self.login_ok:
            print 'login_ok', self.login_ok
            self.sessionid = self.set_cookie('session')
            print 'session', self.sessionid
            if self.sessionid:
                self.login_ok = True
        if isinstance(self.datos, dict):
            version = float(self.datos['version'])
            if os.name != 'nt' and self.version < version:
                mensaje = 'Hay una nueva versión de TCONTUR disponible\n'
                mensaje += '¿Desea instalarla?'
                dialogo = Widgets.Alerta_SINO('Actualización Pendiente',
                            'update.png', mensaje, False)
                respuesta = dialogo.iniciar()
                dialogo.cerrar()
                if respuesta:
                    self.update()
            return True
        else:
            titulo = 'Elija una empresa'
            imagen = 'dar_prioridad.png'
            mensaje = '¿A qué empresa desea ingresar?'
            dialogo = Widgets.Alerta_Combo(titulo, imagen, mensaje, self.datos)
            dialogo.set_focus(dialogo.but_ok)
            respuesta = dialogo.iniciar()
            dialogo.cerrar()
            if respuesta:
                self.empresa = respuesta
                return self.send_login(login)

    def update(self):
        sh.git.stash()
        s = sh.git.pull()
        if s == 'Already up-to-date.\n':
            titulo = 'No hay cambios'
            mensaje = 'No se encontraron actualizaciones disponibles.'
        else:
            titulo = 'Actualizacion Correcta'
            mensaje = 'El programa se ha actualizado correctamente.\n'
            mensaje += 'Reinicie el programa para que los cambios surjan efecto.'
        Widgets.Alerta(titulo, 'update.png', mensaje)

    def set_cookie(self, key):
        cookies = self.req.getheaders()['set-cookie']
        i = cookies.find(key)
        if i == -1:
            return False
        cook = cookies[i:]
        n = cook.find('=')
        m = cook.find(';')
        cookie = cook[n + 1: m]
        self.headers['Cookie'] += '%s=%s; ' % (key, cook[n + 1:m])
        return cookie

    def load(self, consulta, datos={}):
        if self.backup and consulta in self.backup_urls:
            try:
                js = self.get_backup(consulta, datos)
            except IOError:
                print ' No existe backup'
            else:
                print '+++++++++'
                print consulta
                print '+++++++++'
                print datos
                print js
                print '+++++++++'
                if js:
                    return js
        get = datos == {}
        post_data = ''
        if not get:
            datos['iglesia_id'] = self.iglesia_id
            datos['version'] = self.version
            datos['csrfmiddlewaretoken'] = self.csrf
            datos['session'] = self.sessionid
            keys = datos.keys()
            keys.sort()
            for k in keys:
                if datos[k] is None:
                    pass
                elif isinstance(datos[k], tuple) or isinstance(datos[k], list):
                    for d in datos[k]:
                        post_data += '%s=%s&' % (k, d)
                else:
                    post_data += '%s=%s&' % (k, datos[k])
            post_data = post_data[:-1]
        url = '%s%s/' % (self.server, consulta)
        l = len(post_data)
        self.headers['Content-Length'] = str(l)
        try:
            if get:
                r = self.conn.urlopen('HEAD', url, headers=self.headers, assert_same_host=False)
            else:
                r = self.conn.urlopen('POST', url, body=post_data, headers=self.headers, assert_same_host=False)
        except:
            print ('********************************')
            print (url)
            print ('********************************')
            Widgets.Alerta('Error', 'error_envio.png',
                'No es posible conectarse al servidor,\n' +
                'asegúrese de estar conectado a internet\n' +
                'e intente de nuevo.')
            return False
        self.req = r
        a = os.path.abspath('outs/index.html')
        f = open(a, 'wb')
        f.write(r.data)
        f.close()
        if get:
            return True
        try:
            js = json.loads(r.data)
        except:
            print ('********************************')
            print (url)
            print ('********************************')
            print 'json', url, post_data, self.headers
            print r.status
            for v in self.ventanas:
                v.status_bar.push('Error de conexion')
            return False
        return self.ejecutar(js)

    def ejecutar(self, js):
        if len(js) < 2:
            #return True
            return False
        primero = js[0]
        segundo = js[1]
        if primero == 'Json':
            return segundo
        if primero == 'Dialogo':
            Widgets.Alerta('Aviso', 'info.png', segundo)
            for v in self.ventanas:
                v.status_bar.push(segundo.split('\n')[0])
            return self.ejecutar(js[2:])
        elif primero == 'Comando':
            self.comando(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'OK':
            for v in self.ventanas:
                v.status_bar.push(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'Error':
            self.sonido.error()
            Widgets.Alerta('Error', 'error_dialogo.png', segundo)
            for v in self.ventanas:
                v.status_bar.push(segundo.split('\n')[0])
            return False
        elif primero == 'print':
            print 'imprimiendo'
            self.imprimir(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'ticket':
            print 'ticket principal'
            self.ticket(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'open':
            self.open(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'image':
            self.imagen(segundo)
            return self.ejecutar(js[2:])
        elif primero == 'url':
            self.webbrowser(segundo)
            return self.ejecutar(js[2:])
        else:
            return self.ejecutar(js[2:])

    def imprimir(self, datos):
        Impresion.Impresion(datos[0], datos[1])

    def open(self, consulta):
        url = '/%s' % consulta
        self.headers.pop('Content-Length')
        r = self.conn.urlopen('GET', url, headers=self.headers, assert_same_host=False)
        self.req = r
        a = 'outs/reporte.pdf'
        f = open(a, 'wb')
        f.write(r.data)
        f.close()
        a = os.path.abspath(a)
        if os.name == 'nt':
            os.system('start outs/reporte.pdf')
        else:
            os.system("gnome-open outs/reporte.pdf")
        return True

    def imagen(self, consulta):
        url = '/%s' % consulta
        self.headers.pop('Content-Length')
        r = self.conn.urlopen('GET', url, headers=self.headers, assert_same_host=False)
        self.req = r
        a = 'outs/imagen.png'
        f = open(a, 'wb')
        f.write(r.data)
        f.close()
        return True

    def ticket(self, comandos):
        #Widgets.Alerta('Impresion', 'imprimir.png', 'Se ha recibido un ticket para imprimir\nCorte el papel y de un click en Aceptar.')
        self.ticketera.imprimir(comandos)

    def connect(self, string, funcion):
        self.signals[string].append(funcion)

    def emit(self, string):
        for f in self.signals[string]:
            f()

    def nada(self, *args):
        pass

    def comando(self, params):
        funcion = params['funcion']
        default = params['default']
        dialogo = self.http_funciones[funcion](self)
        dialogo.set_defaults(default)
        if dialogo.iniciar():
            self.load(funcion, dialogo.datos)
        dialogo.cerrar()

    def webbrowser(self, url):
        uri = 'http://%s/despacho/ingresar?sessionid=%s&next=%s' % (self.web, self.sessionid, url)
        webbrowser.open(uri)




class Ventana(gtk.Window):

    __gsignals__ = {'cerrar': (gobject.SIGNAL_RUN_LAST,
        gobject.TYPE_NONE, ()),
            'login': (gobject.SIGNAL_RUN_LAST,
        gobject.TYPE_NONE, ()),
            'salidas': (gobject.SIGNAL_RUN_LAST,
        gobject.TYPE_NONE, ())
        }

    def __init__(self, principal, version, dia):
        self.version = version
        super(Ventana, self).__init__()
        pixbuf = gtk.gdk.pixbuf_new_from_file("images/fondo-salida.jpg")
        pixmap, mask = pixbuf.render_pixmap_and_mask()
        width, height = pixmap.get_size()
        del pixbuf
        titulo = 'Sistema de Proyección %s' % version
        self.status_bar = Widgets.Statusbar()
        self.status_bar.push(dia)
        herramientas = [
            ('Sincronizar', 'sincronizar.png', self.sincronizar),
            ('Full Screen', 'fullscreen.png', self.fullscreen),
            ('Limpiar', 'limpiar.png', self.limpiar),
            ('Anterior', 'anterior.png', self.anterior),
            ('Siguiente', 'siguiente.png', self.siguiente),
            ('Fuente +', 'A+.png', self.mas_grande),
            ('Fuente -', 'A-.png', self.menos_grande),
            ]
        toolbar = Widgets.Toolbar(herramientas)
        ticketera = Widgets.Button('imprimir.png', '', 16)
        ticketera.connect('button-press-event', self.ticketera)
        self.menu = gtk.Menu()
        item2 = gtk.MenuItem('LPT1')
        item2.connect('activate', self.impresora_paralela)
        self.menu.append(item2)
        item3 = gtk.MenuItem('Probar')
        item3.connect('activate', self.impresora_probar)
        self.menu.append(item3)
        item4 = gtk.MenuItem('Reimprimir Último')
        item4.connect('activate', self.impresora_reimprimir)
        self.menu.append(item4)
        self.http = principal.http
        self.logueado = False
        self.set_app_paintable(gtk.TRUE)
        self.realize()
        self.window.set_back_pixmap(pixmap, gtk.FALSE)
        self.principal = principal
        #self.http = principal.http
        self.connect('destroy', self.cerrar)
        #Maquetación
        self.set_border_width(2)
        self.set_title(titulo)
        self.set_position(gtk.WIN_POS_CENTER)
        main_vbox = gtk.VBox(False, 0)
        path = os.path.join('images', 'icono.png')
        icon = gtk.gdk.pixbuf_new_from_file(path)
        self.set_icon_list(icon)
        #main_vbox.pack_start(toolbar, False, False, 0)

        self.add(main_vbox)
        self.toolbar = toolbar
        # self.toolbar.add_button('Anterior (Ctrl + F)', 'izquierda.png', self.anterior)
        main_vbox.pack_start(self.toolbar, False, False, 0)
        hbox_main = gtk.HBox(False, 2)
        main_vbox.pack_start(hbox_main, True, True, 0)
            #VBox 1
        self.notebook = Widgets.Notebook()
        hbox_main.pack_start(self.notebook, True, True, 0)
        self.notebook.set_tab_pos(gtk.POS_TOP)
        musica_hbox = gtk.HBox(True, 0)
        self.notebook.insert_page(musica_hbox, gtk.Label('Música'))
        vbox1 = gtk.VBox(False, 0)
        musica_hbox.pack_start(vbox1, True, True, 0)
        self.notebook.set_homogeneous_tabs(True)
        self.notebook.child_set_property(musica_hbox, 'tab-expand', True)

        self.repertorio = Widgets.TreeViewId('Repertorio', ('NOMBRE', 'ALBUM', 'ARTISTA', 'ULTIMA', '*LETRAS'))
        vbox1.pack_start(self.repertorio, True, True, 0)
        self.repertorio.scroll.set_size_request(200, 400)
        self.seleccionados = Widgets.TreeViewId('Seleccionados', ('NOMBRE', 'ALBUM', 'ARTISTA', 'ULTIMA', '*LETRAS'))
        vbox1.pack_start(self.seleccionados, True, True, 0)
        self.seleccionados.scroll.set_size_request(200, 200)
            #VBox 2
        vbox2 = gtk.VBox(False, 0)
        musica_hbox.pack_start(vbox2, True, True, 0)
                #Notebook
        self.parrafos = Widgets.TreeViewId('Párrafos', ('LETRAS',))
        vbox2.pack_start(self.parrafos, True, True, 0)
        self.parrafos.scroll.set_size_request(200, 400)
                #Vueltas

        biblia_hbox = gtk.HBox(True, 0)
        self.notebook.insert_page(biblia_hbox, gtk.Label('Biblia'))
        vbox3 = gtk.VBox(False, 0)
        biblia_hbox.pack_start(vbox3, True, True, 0)
        frame = Widgets.Frame('Versículos')
        vbox = gtk.VBox(False, 0)
        self.version = Widgets.ComboBox()
        self.version.set_lista((('RVR60', 1),))
        vbox.pack_start(self.version, False, False, 0)
        self.entry_versiculo = Widgets.Texto(32)
        self.entry_versiculo.set_size_request(175, 25)
        self.entry_versiculo.connect('activate', self.buscar_versiculo)
        hbox = gtk.HBox(False, 0)
        vbox.pack_start(hbox, False, False, 0)
        hbox.pack_start(self.entry_versiculo, False, False, 0)
        boton = Widgets.Button('mostrar.png', '', 24, self.mostrar_versiculo)
        hbox.pack_start(boton, False, False, 0)
        self.text_versiculo = Widgets.TextView()
        self.text_versiculo.set_size_request(200, 200)
        vbox.pack_start(self.text_versiculo, True, True, 0)
        frame.add(vbox)
        vbox3.pack_start(frame, False, False, 0)



            #VBox3
        vbox_fija = gtk.VBox(False, 0)
        hbox_main.pack_start(vbox_fija, False, False, 0)
        frame = Widgets.Frame('Vista Previa')
        if os.name == 'nt':
            url = 'local/proyeccion.html'
            self.vista_previa = Chrome.Browser(url, 200, 200)
            self.proyector  = Chrome.Window(url)
        else:
            url = 'file:///home/daniel/Python/Proyeccion/local/proyeccion.html'
            self.vista_previa = Chrome.IFrame(url, 200, 200)
            self.proyector = Chrome.Window(url)
        frame.add(self.vista_previa)
        vbox_fija.pack_start(frame, False, False, 0)
        frame = Widgets.Frame('Edición')
        vbox = gtk.VBox(False, 0)
        frame.add(vbox)
        self.text_mostrar = Widgets.TextView()
        self.text_mostrar.set_size_request(200, 200)
        vbox.pack_start(self.text_mostrar, False, False, 0)
        hbox = gtk.HBox(False, 0)
        vbox.pack_start(hbox, False, False, 0)
        boton = Widgets.Button('mostrar.png', 'Mostrar', 24, self.mostrar_edicion)
        hbox.pack_start(boton, False, False, 0)
        boton = Widgets.Button('guardar.png', 'Guardar', 24, self.guardar_edicion)
        hbox.pack_start(boton, False, False, 0)
        vbox_fija.pack_start(frame, False, False, 0)
        self.model = gtk.ListStore(gtk.gdk.Pixbuf, str)
        self.imagenes = Widgets.TreeViewId('Imágenes', ('V.Previa', 'Archivo'))
        self.imagenes.set_liststore((gtk.gdk.Pixbuf, str))
        self.imagenes.escribir((
            [gtk.gdk.pixbuf_new_from_file_at_size('images/fondos/biblia.jpg', 50, 50), 'biblia.jpg'],
            [gtk.gdk.pixbuf_new_from_file_at_size('images/fondos/worship.jpg', 50, 50), 'worship.jpg'],
            ))
        #self.imagenes = gtk.FileChooserWidget()
        vbox_fija.pack_start(self.imagenes, False, False, 0)
        self.imagenes.scroll.set_size_request(200, 200)
        frame = Widgets.Frame('OSD')
        hbox = gtk.HBox(False, 0)
        self.entry_OSD = Widgets.Texto(64)
        self.entry_OSD.set_size_request(200, 25)
        hbox.pack_start(self.entry_OSD, True, True, 0)
        boton = Widgets.Button('mostrar.png', '', 24, self.mostrar_OSD)
        hbox.pack_start(boton, False, False, 0)
        frame.add(hbox)
        vbox_fija.pack_start(frame, False, False, 0)
        self.show_all()
        self.vista_previa.open(url)
        try:
            f = file('outs/escogidas.js', 'rb')
            escogidas = json.loads(f.read())
        except:
            self.http.escogidas = []
        else:
            print('ESCOGIDAS')
            self.http.escogidas = escogidas
            self.seleccionados.escribir(escogidas)
            f.close()
        try:
            f = file('outs/repertorio.js', 'rb')
            repertorio = json.loads(f.read())
        except:
            self.http.repertorio = []
        else:
            print('REPERTORIO')
            self.http.repertorio = repertorio
            self.repertorio.escribir(repertorio)
            f.close()
        self.repertorio.connect('activado', self.usar_cancion)
        self.seleccionados.connect('activado', self.mostrar_letras)
        self.parrafos.connect('activado', self.proyectar_letras)
        self.imagenes.connect('activado', self.cambiar_fondo)
        self.repertorio_id = None
        self.fullscreen = False
        self.biblia = Biblia()

    def login(self, *args):
        return
        self.vista_previa.open('http://%s/musica/login?session=%s&next=/musica/multimedia' % (self.http.dominio, self.http.sessionid))
        self.proyector.open('http://%s/musica/login?session=%s&next=/musica/multimedia' % (self.http.dominio, self.http.sessionid))

    def sincronizar(self, *args):
        datos = self.http.load('sincronizar', {'nada': 1})
        self.http.repertorio = datos['repertorio']
        self.http.escogidas = datos['escogidas']
        self.repertorio.escribir(self.http.repertorio)
        self.seleccionados.escribir(self.http.escogidas)
        self.backup_escogidas()
        f = file('outs/repertorio.js', 'wb')
        f.write(json.dumps(self.http.repertorio))
        f.close()

    def fullscreen(self, *args):
        if self.fullscreen:
            try:
                self.proyector.unfullscreen()
            except:
                url = 'http://%s/musica/login?session=%s&next=/musica/multimedia' % (self.http.dominio, self.http.sessionid)
                self.proyector = Chrome.Window(url)
                self.fullscreen = False
            else:
                self.fullscreen = False
        else:
            try:
                self.proyector.fullscreen()
            except:
                url = 'http://%s/musica/login?session=%s&next=/musica/multimedia' % (self.http.dominio, self.http.sessionid)
                self.proyector = Chrome.Window(url)
                self.fullscreen = False
            else:
                self.fullscreen = True

    def buscar_versiculo(self, *args):
        self.biblia.version(self.version.get_text())
        cita = self.entry_versiculo.get_text()
        texto = self.biblia.get(cita)
        self.text_mostrar.set_text(texto)
        self.enviar_proyector()

    def limpiar(self, *args):
        self.text_mostrar.set_text('')
        self.enviar_proyector()

    def mas_grande(self, *args):
        self.proyector.execute_script('masgrande();')

    def menos_grande(self, *args):
        self.proyector.execute_script('menosgrande();')

    def backup_escogidas(self):
        f = file('outs/escogidas.js', 'wb')
        f.write(json.dumps(self.http.escogidas))
        f.close()

    def usar_cancion(self, widget, fila):
        self.seleccionados.model.append(fila)
        self.http.escogidas.append(list(fila))
        self.backup_escogidas()

    def mostrar_letras(self, widget, fila):
        letras = fila[len(fila) - 2]
        i = 0
        data = []
        for p in letras.split('\r\n\r\n'):
            i += 1
            data.append((p, i))
        self.parrafos.escribir(data)
        self.repertorio_id = fila[len(fila) - 1]
        self.parrafo_id = None

    def cambiar_fondo(self, widget, fila):
        print(fila[0], fila[1])
        self.vista_previa.execute_script('fondo("%s");' % fila[1])
        self.proyector.execute_script('fondo("%s");' % fila[1])

    def proyectar_letras(self, widget, fila):
        letras = fila[0]
        self.text_mostrar.set_text(letras)
        self.parrafo_id = fila[1]
        self.enviar_proyector()

    def enviar_proyector(self):
        letras = self.text_mostrar.get_text()
        letras = letras.replace('\r\n', '</br>')
        letras = letras.replace('\r', '</br>')
        letras = letras.replace('\n', '</br>')
        self.vista_previa.execute_script('escribir("%s")' % letras)
        self.proyector.execute_script('escribir("%s")' % letras)

    def anterior(self, *args):
        texto = self.biblia.anterior()
        self.text_mostrar.set_text(texto)
        self.enviar_proyector()

    def siguiente(self, *args):
        texto = self.biblia.siguiente()
        self.text_mostrar.set_text(texto)
        self.enviar_proyector()

    def mostrar_edicion(self, *args):
        self.enviar_proyector()

    def guardar_edicion(self, *args):
        if self.parrafo_id is None:
            return Widgets.Alerta('error.png', 'No ha seleccionado el párrafo a corregir.')
        texto = self.text_mostrar.get_text()
        self.parrafos.modificar('LETRAS', self.parrafo_id, texto)
        self.enviar_proyector()
        letras = ''
        for p in self.parrafos.model:
            letras += p[0] + '\r\n\r\n'
        self.seleccionados.modificar('*LETRAS', self.repertorio_id, letras)
        self.repertorio.modificar('*LETRAS', self.repertorio_id, letras)
        datos = {
            'letras': letras[0:-4],
            'repertorio_id': self.repertorio_id
        }
        self.backup_escogidas()
        self.http.load('guardar-letras', datos)

    def mostrar_versiculo(self, *args):
        pass
    def mostrar_OSD(self, *args):
        pass

    def ticketera(self, widgets, event):
        if event.button == 1:
            x = int(event.x)
            y = int(event.y)
            t = event.time
            self.menu.popup(None, None, None, event.button, t)
            self.menu.show_all()
            return True

    def impresora_serial(self, menu, puerto):
        self.http.ticketera.conectar_serial(puerto)

    def impresora_paralela(self, *args):
        self.http.ticketera.paralela()

    def impresora_probar(self, *args):
        self.http.ticketera.probar()

    def impresora_reimprimir(self, *args):
        self.http.ticketera.reimprimir()

    def cerrar(self, *args):
        self.destroy()



if __name__ == '__main__':
    s = Splash()
    gtk.main()
    infinito = False
    if os.name == 'nt':
        os.system('taskkill /im Proyeccion.exe /f')
    Chrome.close()
